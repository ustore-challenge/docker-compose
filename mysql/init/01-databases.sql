# create databases
CREATE DATABASE IF NOT EXISTS `sso-db`;
CREATE DATABASE IF NOT EXISTS `sso-db-test`;
CREATE DATABASE IF NOT EXISTS `manager-db`;

# create root user and grant rights
CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';